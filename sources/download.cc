
#include "download.h"
#include <curl/curl.h>
#include <tidy.h>
#include <tidybuffio.h>
#include <memory>
#include <sstream>

struct CURL_Deleter {
    void operator()(CURL *x) const noexcept
        { curl_easy_cleanup(x); }
};

struct CURLSH_Deleter {
    void operator()(CURLSH *x) const noexcept
        { curl_share_cleanup(x); }
};

static std::unique_ptr<CURLSH, CURLSH_Deleter> curl_share;

bool download_file_to_stream(const char *url, std::ostream &os)
{
    CURLSH *sh = curl_share.get();
    if (!sh) {
        sh = curl_share_init();
        if (!sh)
            throw std::bad_alloc();
        curl_share.reset(sh);
        curl_share_setopt(sh, CURLSHOPT_SHARE, CURL_LOCK_DATA_COOKIE|CURL_LOCK_DATA_DNS|CURL_LOCK_DATA_SSL_SESSION|CURL_LOCK_DATA_CONNECT|CURL_LOCK_DATA_PSL);
    }

    std::unique_ptr<CURL, CURL_Deleter> curl(curl_easy_init());
    if (!curl)
        throw std::bad_alloc();

    curl_easy_setopt(curl.get(), CURLOPT_SHARE, sh);
    curl_easy_setopt(curl.get(), CURLOPT_URL, url);
    curl_easy_setopt(curl.get(), CURLOPT_FOLLOWLOCATION, 1l);

    curl_easy_setopt(curl.get(), CURLOPT_WRITEFUNCTION,
        +[](const char *ptr, size_t size, size_t nmemb, std::ostream *os) -> size_t
             { return os->write(ptr, nmemb * size) ? nmemb : 0; });
    curl_easy_setopt(curl.get(), CURLOPT_WRITEDATA, &os);

    curl_easy_perform(curl.get());

    long response_code;
    if (curl_easy_getinfo(curl.get(), CURLINFO_RESPONSE_CODE, &response_code) != CURLE_OK ||
        response_code != 200)
        return false;

    return true;
}

bool download_file_to_string(const char *url, std::string &str)
{
    std::ostringstream os(std::ios::binary);
    if (!download_file_to_stream(url, os))
        return false;

    str = os.str();
    return true;
}

struct Tidy_Deleter {
    void operator()(TidyDoc doc) const noexcept
        { tidyRelease(doc); }
    void operator()(TidyBuffer *buf) const noexcept
        { tidyBufFree(buf); delete buf; }
};

bool download_file_to_document(const char *url, pugi::xml_document &doc)
{
    std::string str;
    if (!download_file_to_string(url, str))
        return false;

    std::unique_ptr<const _TidyDoc, Tidy_Deleter> tidydoc(tidyCreate());
    tidyOptSetBool(tidydoc.get(), TidyXhtmlOut, Bool::yes);
    tidyOptSetBool(tidydoc.get(), TidyShowWarnings, Bool::no);
    tidyOptSetBool(tidydoc.get(), TidyShowErrors, Bool::no);
    tidyOptSetInt(tidydoc.get(), TidyWrapLen, 0);
    tidyOptSetBool(tidydoc.get(), TidyForceOutput, Bool::yes);
    tidyParseString(tidydoc.get(), str.c_str());

    std::unique_ptr<TidyBuffer, Tidy_Deleter> tidybuf(new TidyBuffer);
    tidyBufInit(tidybuf.get());
    tidyBufAlloc(tidybuf.get(), 2 * str.size());
    tidySaveBuffer(tidydoc.get(), tidybuf.get());

    if (false) {
        std::cout.write((char *)tidybuf->bp, tidybuf->size);
        std::cout << std::endl;
    }

    if (!doc.load_buffer(tidybuf->bp, tidybuf->size))
        return false;

    return true;
}
