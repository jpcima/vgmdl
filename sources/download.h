
#include <pugixml.hpp>
#include <string>
#include <iostream>
#include <memory>

bool download_file_to_stream(const char *url, std::ostream &os);
bool download_file_to_string(const char *url, std::string &str);
bool download_file_to_document(const char *url, pugi::xml_document &doc);
