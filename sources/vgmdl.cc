#include "download.h"
#include <string>
#include <vector>
#include <memory>
#include <fstream>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
///
static std::string download_directory;

///
struct Platform {
    std::string name;
    std::string url;
};

struct Category {
    std::string name;
    std::vector<Platform> platforms;
};

struct Main_Index {
    std::vector<Category> cats;
};

struct Track_Info {
    std::string url;
    std::string game;
    std::string track;
    std::string file;
    const Category *cat = nullptr;
    const Platform *pf = nullptr;
};

///
static std::string sanitize(std::string in)
{
    for (char &c : in) {
        if (c == '/') c = '_';
#if defined(_WIN32)
        if (c == '\\') c = '_';
#endif
    }
    if (in == ".")
        in[0] = '_';
    else if (in == "..") {
        in[0] = '_';
        in[1] = '_';
    }
    return in;
}

///
bool vgmdl_add_platform(Category &cat, pugi::xml_node opt)
{
    cat.platforms.emplace_back();
    Platform &pf = cat.platforms.back();
    pf.name = opt.attribute("label").value();
    if (pf.name.empty())
        pf.name = opt.text().as_string();
    if (pf.name.empty()) {
        fprintf(stderr, "Format error\n");
        return false;
    }

    // fprintf(stderr, "** %s\n", pf.name.c_str());

    pf.url = opt.attribute("value").value();
    if (pf.url.empty()) {
        fprintf(stderr, "Format error\n");
        return false;
    }

    return true;
}

bool vgmdl_do_main_page(Main_Index &idx)
{
    fprintf(stderr, "Main page\n");

    idx = Main_Index();

    pugi::xml_document doc;
    if (!download_file_to_document("https://vgmusic.com/", doc)) {
        fprintf(stderr, "Download error\n");
        return false;
    }

    pugi::xpath_node_set set =
        doc.select_nodes("//select[@name='system']/optgroup");

    for (size_t i = 0, n = set.size(); i < n; ++i) {
        pugi::xml_node optgroup = set[i].node();

        idx.cats.emplace_back();
        Category &cat = idx.cats.back();
        cat.name = optgroup.attribute("label").value();

        // fprintf(stderr, "%s\n", cat.name.c_str());

        pugi::xpath_node_set set2 = optgroup.select_nodes("option");
        for (size_t j = 0, m = set2.size(); j < m; ++j) {
            if (!vgmdl_add_platform(cat, set2[j].node()))
                return false;
        }
    }

    set = doc.select_nodes("//select[@name='system']/option");
    if (!set.empty()) {
        Category &cat = idx.cats.back();
        cat.name = "Others";

        for (size_t j = 0, m = set.size(); j < m; ++j) {
            if (!vgmdl_add_platform(cat, set[j].node()))
                return false;
        }
    }

    return true;
}

static bool vgmdl_do_platform(
    const Category &cat, const Platform &pf,
    std::vector<std::unique_ptr<Track_Info>> &tracks)
{
    fprintf(stderr, "Platform %s/%s\n", cat.name.c_str(), pf.name.c_str());

    pugi::xml_document doc;
    if (!download_file_to_document(pf.url.c_str(), doc)) {
        fprintf(stderr, "Download error '%s'\n", pf.url.c_str());
        return false;
    }

    pugi::xml_node table = doc.select_node("//table").node();

    pugi::xpath_node_set table_headers = table.select_nodes("tr[@class='header']");

    for (size_t i = 0, n = table_headers.size(); i < n; ++i) {
        pugi::xml_node header_row = table_headers[i].node();

        std::string game_name;
        pugi::xml_node game_title_link = header_row.first_child().first_child();
        if (!strcmp(game_title_link.name(), "a"))
            game_name = game_title_link.text().as_string();
        if (game_name.empty())
            continue;

        for (pugi::xml_node next = header_row.next_sibling();
            (next && ((i + 1 == n) || next != table_headers[i + 1].node()));
            next = next.next_sibling())
        {
            pugi::xml_node link = next.first_child().first_child();

            if (strcmp(link.name(), "a"))
                continue;

            std::unique_ptr<Track_Info> info(new Track_Info);
            info->cat = &cat;
            info->pf = &pf;
            info->game = game_name;
            info->track = link.text().as_string();
            info->file = link.attribute("href").value();
            if (info->track.empty() || info->file.empty())
                continue;

            info->url = pf.url;
            if (info->url.back() != '/')
                info->url.push_back('/');
            info->url += info->file;

            tracks.push_back(std::move(info));
        }
    }

    return true;
}

static bool do_download(const Track_Info &info, size_t index, size_t count)
{
    std::string download_directory = ::download_directory
        + '/' + sanitize(info.cat->name) + '/' + sanitize(info.pf->name);
    mkdir(download_directory.c_str(), 0755);

    std::string game_download_directory = download_directory + '/' + sanitize(info.game);
    mkdir(game_download_directory.c_str(), 0755);

    std::string track_download_path = game_download_directory + '/' + sanitize(info.file);

    struct stat st;
    if (stat(track_download_path.c_str(), &st) == 0)
        return true;

    fprintf(stderr, "[%zu/%zu] Download %s to %s\n",
            index + 1, count,
            info.url.c_str(), track_download_path.c_str());

    std::string track_partial_path = track_download_path + ".part";
    std::ofstream os(track_partial_path, std::ios::binary);

    if (!download_file_to_stream(info.url.c_str(), os)) {
        fprintf(stderr, "Download error\n");
        unlink(track_partial_path.c_str());
        return false;
    }

    os.flush();
    if (!os) {
        fprintf(stderr, "Output error\n");
        unlink(track_partial_path.c_str());
        return false;
    }

    rename(track_partial_path.c_str(), track_download_path.c_str());
    return true;
}

///
int main(int argc, char *argv[])
{
    if (argc < 2) {
        fprintf(stderr, "Usage: vgmdl <download-directory>\n");
        return 0;
    }

    if (argc != 2) {
        fprintf(stderr, "Please indicate the download directory.\n");
        return 1;
    }

    download_directory.assign(argv[1]);
    mkdir(download_directory.c_str(), 0755);

    struct stat st;
    if (stat(download_directory.c_str(), &st) != 0 || !S_ISDIR(st.st_mode)) {
        fprintf(stderr, "Could not create the download directory.\n");
        return 1;
    }

    ///

    Main_Index idx;

    if (!vgmdl_do_main_page(idx))
        return 1;

    std::vector<std::unique_ptr<Track_Info>> tracks;

    for (Category &cat : idx.cats) {
        mkdir((download_directory + '/' + sanitize(cat.name)).c_str(), 0755);
        for (Platform &pf : cat.platforms) {
            if (!vgmdl_do_platform(cat, pf, tracks))
                return 1;
        }
    }

    for (size_t i = 0, n = tracks.size(); i < n; ++i) {
        const Track_Info &info = *tracks[i];
        bool success = do_download(info, i, n);
        // if (!success)
        //     return 1;
    }

    return 0;
}
